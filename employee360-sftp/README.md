# EMPLOYEE360 API CONSUMPTION BY READING A FILE IN REMOTE SERVER USING SFTP#

### How do I get set up? ###

* Install python3 on your platform
* cd into employee360-sftp folder
* Run pip install -r requirements.txt to install dependencies to run this package
* Fill in the details in config.json file before running the python script
* * mapping -> 
    * columns in csv file to be mapped to subject, evaluators and approvers of api
    * Please refer empployee350 api for more info
* * surveyId -> surveyId of employee360 survey
* * TOKEN -> The Custom App Token of your surveysparrow account with required privileges
* * ftp
    * host -> ftp hosted ip
    * username -> username of ftp user
    * password -> password of ftp user
    * private_key -> 
        * location of the private key file ( valid only if SSH layer is there ), 
        * if SSH is not there, please keep the ".ppk" value
    * host_keys -> (please refer ssh docs and paramiko python library)
    * remoteFilePath -> location of the file in the remote file server

# Note
    Please refer "ssh" docs and "paramiko" pyhton library docs for more info on using sftp over ssh.
    Please feel free to make changes to the code as you wish 

* Run command "pyhton main.py" to fetch the file, parse according to config.json and send out invites.
