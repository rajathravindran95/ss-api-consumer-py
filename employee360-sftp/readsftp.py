import pysftp
import csv
import json
from employee360api import createInvite


def readFile():
    try:

        with open('employee360-sftp/config.json') as configFile:
            data = json.load(configFile)
            mappingDict = data['mapping']
            surveyId = data['surveyId']
            TOKEN = data['TOKEN']
            ftp = data['ftp']

        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = ftp['hostkeys']
        s = pysftp.Connection(host=ftp['host'], username=ftp['username'],
                              password=ftp['password'], private_key=ftp['private_key'], cnopts=cnopts)
        s = pysftp.Connection(host=ftp['host'], username=ftp['username'],
                              password=ftp['password'], private_key=ftp['private_key'], cnopts=cnopts)
        s.chdir('.')
        with s.open(ftp['remoteFilePath'], 'r') as remoteFile:
            remoteFile.prefetch()
            csvData = remoteFile.readlines()
            reader = csv.DictReader(csvData)
            for row in reader:
                createInvite(surveyId, TOKEN, mappingDict, row)
        s.close()

    except Exception as err:
        print(err)
