import requests

def generateEmployee360Payload(csvRowData, mappingDict):
    subjectMapping = None
    evaluatorMapping = None
    approverMapping = None
    payload = {}

    if 'subject' in mappingDict:
        subjectMapping = mappingDict['subject']

    if 'evaluator' in mappingDict:
        evaluatorMapping = mappingDict['evaluator']

    if 'approver' in mappingDict:
        approverMapping = mappingDict['approver']

    if subjectMapping:
        subject = {}
        subject['email'] = csvRowData[subjectMapping['email']]
        if 'fullName' in subjectMapping:
            subject['fullName'] = csvRowData[subjectMapping['fullName']]
        payload['subject'] = subject

    if evaluatorMapping:
        evaluator = {}
        evaluator['email'] = csvRowData[evaluatorMapping['email']]
        evaluator['relation'] = csvRowData[evaluatorMapping['relation']]
        if 'fullName' in evaluatorMapping:
            evaluator['fullName'] = csvRowData[evaluatorMapping['fullName']]
        payload['evaluators'] = [evaluator]

    if approverMapping:
        approver = {}
        approver['email'] = csvRowData[approverMapping['email']]
        if 'fullName' in approverMapping:
            approver['fullName'] = csvRowData[approverMapping['fullName']]
        payload['approver'] = approver
    
    payload['properties'] = mappingDict['properties']
    return payload


def createInvite(surveyId, TOKEN, mappingDict, csvRowData):
    try:

        headers = {'Authorization': 'Bearer {}'.format(
            TOKEN), 'Content-Type': 'application/json'}
        
        payload = generateEmployee360Payload(csvRowData, mappingDict)

        response = requests.post(
            'https://api.surveysparrow.com/v1/survey/{}/invite'.format(
                surveyId),
            json=payload,
            headers=headers
        )
        if response.status_code != 200:
            raise Exception(response.text)
        print('Invite Sent, payload = {}'.format(payload) )
    except Exception as err:
        print('Error: sending invite to  {}, err: {}'.format(payload['subject']['email'], err))
        

